resource "helm_release" "event-streaming" {
  name  = "event-streaming"
  chart = "../event-streaming-service/helm"
}

resource "helm_release" "backend" {
  name  = "backend"
  chart = "../mini_backend/helm"
}

