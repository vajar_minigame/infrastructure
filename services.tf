resource "kubernetes_service" "backend" {
  metadata {
    name = "backend-service"
  }
  spec {
    selector = {
      app = "backend"
    }
    port {
      name = "grpc"
      port = 8080
      #node_port = 30800
    }

  }
}

resource "kubernetes_service" "battle" {
  metadata {
    name = "battle-service"
  }
  spec {
    selector = {
      app = "battle"
    }
    port {
      name = "manage"
      port = 8080
    }

  }
}

resource "kubernetes_service" "event-streaming" {
  metadata {
    name = "event-streaming-service"
  }
  spec {
    selector = {
      app = "event-streaming"
    }
    port {
      name = "grpc"
      port = 8080
    }
  }
}
