resource "kubernetes_deployment" "nats" {
  metadata {
    name = "nats"
    labels = {
      app = "nats"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "nats"
      }
    }

    template {
      metadata {
        labels = {
          app = "nats"
        }
      }

      spec {
        container {
          image = "nats:latest"
          name  = "nats"

          port {
            container_port = 4444
          }
          port {
            name           = "client"
            container_port = 4222
          }
          port {
            name           = "manage"
            container_port = 8222
          }
          port {
            name           = "cluster"
            container_port = 6222
          }

          resources {
            limits {
              cpu    = 1
              memory = "64Mi"
            }
          }
        }
      }
    }
  }
}



resource "kubernetes_service" "nats" {
  metadata {
    name = "nats"
  }
  spec {
    selector = {
      app = "${kubernetes_deployment.nats.metadata.0.labels.app}"
    }
    port {
      name = "manage"
      port = 8222
      //  node_port = 30822
    }

    port {
      name      = "client"
      port      = 4222
      node_port = 30422
    }

    port {
      name = "cluster"
      port = 6222
      // node_port = 30622
    }


    type = "NodePort"
  }
}


################envoy############


resource "kubernetes_config_map" "grpc-proxy" {
  metadata {
    name = "grpc-proxy-config"
  }

  data = {
    "envoy.yaml" = "${file("${path.module}/envoy/envoy.yaml")}"
  }
}

resource "kubernetes_deployment" "grpc-proxy" {
  metadata {
    name = "grpc-proxy"
    labels = {
      app = "grpc-proxy"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "grpc-proxy"
      }
    }

    template {
      metadata {
        labels = {
          app = "grpc-proxy"
        }
      }

      spec {
        container {
          image = "envoyproxy/envoy:v1.14.1"
          name  = "grpc-proxy"

          port {

            container_port = 8080
          }

          port {
            name           = "admin"
            container_port = 9901
          }
          command = ["/usr/local/bin/envoy"]
          args    = ["-c /etc/envoy/envoy.yaml", "-l", "trace"]

          volume_mount {
            name       = "config"
            mount_path = "/etc/envoy/"
          }

          resources {
            limits {
              cpu    = 1
              memory = "64Mi"
            }
          }



        }


        volume {
          name = "config"
          config_map {
            name = kubernetes_config_map.grpc-proxy.metadata.0.name

          }


        }

      }
    }
  }
}




resource "kubernetes_service" "grpc-proxy" {
  metadata {
    name = "grpc-proxy"
  }
  spec {
    selector = {
      app = "${kubernetes_deployment.grpc-proxy.metadata.0.labels.app}"
    }
    port {
      name      = "manage"
      port      = 8080
      node_port = 30808
    }


    type = "NodePort"
  }
}





