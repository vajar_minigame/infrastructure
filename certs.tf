resource "tls_private_key" "root-cert" {
  algorithm   = "RSA"
  ecdsa_curve = "4096"
}


resource "kubernetes_secret" "root-cert-k8s" {
  metadata {
    name = "root-cert"
  }

  data = {
    "root-cert" = tls_private_key.root-cert.private_key_pem
  }

}
